import { useEffect, useState } from "react";
import "./App.css";
import { Card } from "@material-ui/core";

const App = () => {
  const [user, setUser] = useState(false);
  const [url, setUrl] = useState("");
  const [botao, setBotao] = useState(true);
  const handleToggle = () => {
    setUser(!user);
    setBotao(!botao);
  };
  useEffect(() => {
    fetch("https://api.github.com/users/francisfuzz")
      .then((response) => response.json())
      .then((response) => setUrl(response));
    console.log(url);
  }, []);

  return (
    <div className="App">
      <button onClick={() => handleToggle()}>
        {botao ? "mostrar" : "esconder"}
      </button>
      <Card id="card">
        {user && <img src={url.avatar_url}></img>}
        {user && <div>{url.name}</div>}
        {user && <div>{url.location}</div>}
        {user && <div>{url.bio}</div>}
      </Card>
    </div>
  );
};

export default App;
